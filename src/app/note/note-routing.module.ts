import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { extract } from '@app/core';
import { NoteComponent } from './note.component';

const routes: Routes = [
  { path: 'note', component: NoteComponent, data: { title: extract('Note') } }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class NoteRoutingModule {}
