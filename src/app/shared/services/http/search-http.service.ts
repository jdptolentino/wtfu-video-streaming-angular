import { Injectable } from '@angular/core';

import { HttpService } from './http.services';
import { Series } from '@app/shared/models/db/series.model';

@Injectable()
export class SearchHttpService extends HttpService<any> {
  private relativeUrl = '/search';

  all(request: any) {
    return this.getAll(this.relativeUrl, request);
  }

  getBySlug(slug: string, request: any) {
    return this.getAll(this.relativeUrl + '/' + slug, request);
  }
}
