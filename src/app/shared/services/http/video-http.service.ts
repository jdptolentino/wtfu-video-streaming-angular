import { Injectable } from '@angular/core';

import { HttpService } from './http.services';
import { Video } from '@app/shared/models/db/video.model';

@Injectable()
export class VideoHttpService extends HttpService<Video> {
  private relativeUrl = '/videos';

  all(request: any) {
    return this.getAll(this.relativeUrl, request);
  }

  getByCategory(slug: string, request: any) {
    return this.getAll(this.relativeUrl + '/category/' + slug, request);
  }
}
