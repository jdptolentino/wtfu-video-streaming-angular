import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { isEmpty } from 'lodash';

import { environment } from '@env/environment';
import { HttpQueryResponse } from '@app/shared/models/http/http-query-response.model';
import { Observable } from 'rxjs';

@Injectable()
export class HttpService<T> {
  private apiUrl = environment.serverUrl;

  constructor(private http: HttpClient) {}

  protected getAll(
    relativeUrl: string,
    requestParams?: any
  ): Observable<HttpQueryResponse<T>> {
    const url = `${this.apiUrl}${relativeUrl}`;
    return this.http.get(url, { params: requestParams });
  }

  protected get(
    relativeUrl: string,
    id: string,
    requestParams?: any
  ): Observable<T> {
    const url = `${this.apiUrl}${relativeUrl}/${id}`;
    return this.http.get<T>(url, { params: requestParams });
  }

  protected put(
    relativeUrl: string,
    id: string,
    data: any,
    requestParams?: any
  ): Observable<T> {
    const url = isEmpty(id)
      ? `${this.apiUrl}${relativeUrl}`
      : `${this.apiUrl}${relativeUrl}/${id}`;
    return this.http.put<T>(url, data, { params: requestParams });
  }

  protected patch(
    relativeUrl: string,
    id: string,
    data: any,
    urlPrefix: string,
    requestParams?: any
  ): Observable<T> {
    const url = isEmpty(id)
      ? `${this.apiUrl}${relativeUrl}/${urlPrefix}`
      : `${this.apiUrl}${relativeUrl}/${id}/${urlPrefix}`;
    return this.http.patch<T>(url, data, { params: requestParams });
  }

  protected delete(relativeUrl: string, id: string): Observable<any> {
    const url = `${this.apiUrl}${relativeUrl}/${id}`;
    return this.http.delete(url);
  }

  protected post(relativeUrl: string, data: any): Observable<T> {
    const url = `${this.apiUrl}${relativeUrl}`;
    return this.http.post<T>(url, data);
  }
}
