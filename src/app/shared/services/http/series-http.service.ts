import { Injectable } from '@angular/core';

import { HttpService } from './http.services';
import { Series } from '@app/shared/models/db/series.model';

@Injectable()
export class SeriesHttpService extends HttpService<Series> {
  private relativeUrl = '/series';

  all(request: any) {
    return this.getAll(this.relativeUrl, request);
  }

  getBySlug(slug: string, request: any) {
    return this.getAll(this.relativeUrl + '/' + slug, request);
  }
}
