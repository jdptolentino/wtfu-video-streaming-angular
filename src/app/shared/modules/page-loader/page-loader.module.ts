import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContentLoaderModule } from '@netbasal/ngx-content-loader';
import { PageLoaderComponent } from './page-loader.component';

@NgModule({
  declarations: [
    // PageLoaderComponent
  ],
  imports: [CommonModule, ContentLoaderModule]
})
export class PageLoaderModule {}
