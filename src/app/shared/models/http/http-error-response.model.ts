export interface HttpErrorResponse {
  error: string;
  message: string;
}

export interface ErrorData {
  error: string;
  message: string;
}
