export interface User {
  id?: number;
  first_name?: string;
  middle_name?: string;
  user_name?: string;
  last_name?: string;
  plan?: string;
  email?: string;
  status?: string;
  subscription?: any;
}
