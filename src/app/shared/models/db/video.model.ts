export interface Video {
  id?: string;
  title?: string;
  link?: string;
  category_id?: string;
  category?: object;
  description?: string;
  updatedAt: Date;
  createAt: Date;
}
