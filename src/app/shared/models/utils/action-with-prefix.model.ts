import { Action } from '@ngrx/store';

export interface ActionWithPayloadPrefix<T> extends Action {
  payload: T;
  actionPrefix: string;
}
