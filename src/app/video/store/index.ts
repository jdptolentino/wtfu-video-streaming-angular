import { createSelector, createFeatureSelector } from '@ngrx/store';
import StoreState from './store.state';
import * as fromFeature from './reducers/store.reducer';

export interface FeatureModuleState {
  video: StoreState;
}

export const reducers = {
  video: fromFeature.StoreReducer
};

export interface AppState extends StoreState {
  video: FeatureModuleState;
}

export const selectFeatureModuleState = createFeatureSelector<
  FeatureModuleState
>('videoModule');

export const selectFeatureState = createSelector(
  selectFeatureModuleState,
  (state: FeatureModuleState) => state.video
);

export const getVideo = createSelector(
  selectFeatureState,
  (state: StoreState) => state.videos
);

export const getLoading = createSelector(
  selectFeatureState,
  (state: StoreState) => state.loading
);
