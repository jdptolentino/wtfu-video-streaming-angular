import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VideoRoutingModule } from './video-routing.module';
import { StoreModule } from '@ngrx/store';
import { reducers } from './store/index';
import { EffectsModule } from '@ngrx/effects';
import { StoreEffects } from './store/effects/store.effect';
import { VideoHttpService } from '@app/shared/services/http/video-http.service';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@app/shared';
import { VideoComponent } from './video.component';
import { EmbedVideo } from 'ngx-embed-video';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CoreModule } from '@app/core';

@NgModule({
  declarations: [VideoComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    CoreModule,
    VideoRoutingModule,
    RouterModule,
    SharedModule,
    StoreModule.forFeature('videoModule', reducers),
    EffectsModule.forFeature([StoreEffects]),
    EmbedVideo.forRoot()
  ],
  providers: [VideoHttpService]
})
export class VideoModule {}
