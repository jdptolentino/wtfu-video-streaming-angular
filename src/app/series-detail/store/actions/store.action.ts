import { createAction, props } from '@ngrx/store';
import { Series } from '@app/shared/models/db/series.model';
import { HttpQueryRequest } from '@app/shared/models/http/http-query-request.model';
import { Video } from '@app/shared/models/db/video.model';

export const loadSeries = createAction(
  '[Series] Load Series',
  props<{ slug: string; request: HttpQueryRequest }>()
);
export const loadSeriesSuccess = createAction(
  '[Series] Load Series Success',
  props<{ series: Series[]; videos: Video[] }>()
);
export const reset = createAction('[Series] Reset Series', props<{}>());
export const resetData = createAction('[Series] Reset Data', props<{}>());

export const failed = createAction(
  '[Series] Failed',
  props<{ payload: any }>()
);
