import { Action, createReducer, on } from '@ngrx/store';
import * as Actions from '../actions/store.action';
import StoreState, { initializeState, adapter } from '../store.state';

export const intialState = initializeState;

const reducer = createReducer(
  intialState,
  on(Actions.loadSeries, (state: StoreState, { request }) => {
    const mergedRequest = request
      ? { ...state.request, ...request }
      : state.request;
    return { ...state, loading: true, request: mergedRequest };
  }),
  on(Actions.loadSeriesSuccess, (state: StoreState, { series, videos }) => {
    state = {
      ...state,
      series,
      videos,
      loading: false
    };
    return adapter.addMany(videos, state);
  }),
  on(Actions.reset, (state: StoreState, {}) => {
    return { ...intialState };
  }),
  on(Actions.resetData, (state: StoreState, {}) => {
    return { videos: [], loading: false };
  }),
  on(Actions.failed, (state: StoreState, {}) => {
    return { ...state, loading: false };
  })
);

export function StoreReducer(state: StoreState | undefined, action: Action) {
  return reducer(state, action);
}
