import { createSelector, createFeatureSelector } from '@ngrx/store';
import StoreState from './store.state';
import * as fromFeature from './reducers/store.reducer';
import { adapter } from './store.state';
export interface FeatureModuleState {
  seriesData: StoreState;
}

export const reducers = {
  seriesData: fromFeature.StoreReducer
};

export interface AppState extends StoreState {
  seriesData: FeatureModuleState;
}

export const selectFeatureModuleState = createFeatureSelector<
  FeatureModuleState
>('seriesModule');

export const selectFeatureState = createSelector(
  selectFeatureModuleState,
  (state: FeatureModuleState) => state.seriesData
);

export const getRequest = createSelector(
  selectFeatureState,
  (state: StoreState) => state.request
);
export const getSeries = createSelector(
  selectFeatureState,
  (state: StoreState) => state.series
);
export const getLoading = createSelector(
  selectFeatureState,
  (state: StoreState) => state.loading
);
export const { selectAll: selectAllData } = adapter.getSelectors(
  selectFeatureState
);
