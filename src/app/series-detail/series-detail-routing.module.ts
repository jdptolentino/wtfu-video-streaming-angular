import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { Shell } from '@app/shell/shell.service';
import { AuthenticationGuard, extract } from '@app/core';
import { SeriesDetailComponent } from './series-detail.component';

const routes: Routes = [
  Shell.childRoutes([
    {
      path: 'series/:slug',
      component: SeriesDetailComponent,
      canActivate: [AuthenticationGuard],
      data: { title: extract('Series') }
    }
  ])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class SeriesDetailRoutingModule {}
