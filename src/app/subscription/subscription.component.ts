import { Component, OnInit } from '@angular/core';
import { SubscriptionService } from './subscription.service';
import { Observable } from 'rxjs';
import { Plan } from '@app/shared/models/db/plan.model';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';
import { PaymentHttpService } from '@app/shared/services/http/payment-http.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-subscription',
  templateUrl: './subscription.component.html',
  styleUrls: ['./subscription.component.scss']
})
export class SubscriptionComponent implements OnInit {
  subscription$: Observable<Plan[]>;
  loading$: Observable<boolean>;
  selectedPlan: Plan;
  step: number = 1;
  selectedMethod: string = '';

  constructor(
    private router: ActivatedRoute,
    private subscriptionService: SubscriptionService,
    private paymentService: PaymentHttpService,
    private http: HttpClient
  ) {
    this.subscription$ = this.subscriptionService.subscription$;
    this.loading$ = this.subscriptionService.loading$;
  }

  ngOnInit(): void {
    this.subscriptionService.loadSubscription();
    this.router.queryParams.subscribe(param => {
      if (param.request && param.request === 'success') {
        this.step = 3;
      }
    });
  }

  handleSelectMethod(plan: Plan): void {
    const auth = JSON.parse(localStorage.getItem('auth'));

    switch (this.selectedMethod) {
      case 'paypal':
        const auth = JSON.parse(localStorage.getItem('auth'));

        this.paymentService
          .pay(auth.user.id, plan.id)
          .subscribe((r: any) => (document.location.href = r.link));
        break;
      case 'other':
        this.subscriptionService.choosePlan(plan.id);

        break;
    }
  }

  selectPaymentMethod(type: string) {
    if (type === 'patreon') {
      this.step = 4;
    } else {
      this.selectedMethod = type;
      this.step = 2;
    }
  }
}
