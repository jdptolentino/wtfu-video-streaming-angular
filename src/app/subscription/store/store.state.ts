import { HttpQueryRequest } from '@app/shared/models/http/http-query-request.model';
import { HttpQueryResponse } from '@app/shared/models/http/http-query-response.model';
import { Plan } from '@app/shared/models/db/plan.model';

export default class StoreState {
  loading: boolean;
  loadingRef: string;
  processing: boolean;
  data: Plan[];
  request: HttpQueryRequest;
  response: HttpQueryResponse<Plan>;
}

export const initializeState = (): StoreState => {
  return {
    loading: false,
    loadingRef: null,
    processing: false,
    data: [],
    request: {
      page: 1,
      perPage: 0
    },
    response: {
      total: 0,
      page: 1,
      perPage: 0,
      data: []
    }
  };
};
