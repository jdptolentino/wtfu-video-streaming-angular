import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import * as StoreActions from './store/actions/store.action';
import * as FromState from './store/index';
import { Store, select } from '@ngrx/store';
import { AppState } from './store/index';
import { Video } from '@app/shared/models/db/video.model';
import { Series } from '@app/shared/models/db/series.model';

/**
 * Provides a base for authentication workflow.
 * The login/logout methods should be replaced with proper implementation.
 */
@Injectable({
  providedIn: 'root'
})
export class HomeService {
  videos$: Observable<Video[]>;
  series$: Observable<Series[]>;
  searches$: Observable<any>;
  loading$: Observable<boolean>;
  request$: any;

  constructor(private store$: Store<AppState>) {
    this.request$ = this.store$.pipe(select(FromState.getRequest));
    this.videos$ = this.store$.pipe(select(FromState.getVideo));
    this.series$ = this.store$.pipe(select(FromState.selectAllData));
    this.loading$ = this.store$.pipe(select(FromState.getLoading));
    this.searches$ = this.store$.pipe(select(FromState.getSearches));
  }

  loadSeries(request: { perPage: number; page: number; search: string }) {
    this.store$.dispatch(StoreActions.loadSeries({ request }));
  }

  search(search: any = '') {
    const data = {
      search: search
    };
    this.store$.dispatch(StoreActions.search({ request: data }));
  }

  loadVideos() {
    const data = {
      perPage: 10,
      page: 1
    };
    this.store$.dispatch(StoreActions.loadVideo({ request: data }));
  }

  reset() {
    this.store$.dispatch(StoreActions.reset({}));
  }
}
