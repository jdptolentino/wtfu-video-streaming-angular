import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectionStrategy
} from '@angular/core';
import { HomeService } from './home.service';
import { Observable } from 'rxjs';
import { Video } from '@app/shared/models/db/video.model';
import { EmbedVideoService } from 'ngx-embed-video';
import { Series } from '@app/shared/models/db/series.model';
import { isEmpty } from 'lodash';
import { AuthenticationService } from '@app/core';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeComponent implements OnInit, OnDestroy {
  quote: string | undefined;
  isLoading = false;
  modalText = '';
  iframe_html: any;
  request$: any;
  videos$: Observable<Video[]>;
  series$: Observable<Series[]>;
  searches$: Observable<any>;
  loading$: Observable<boolean>;
  searchString: string = '';
  plan: any;
  page: number;
  total: number;
  activeRecordsCount: number;

  videos: [];
  series: [];
  request: any;

  initialSeries: [];

  constructor(
    private homeService: HomeService,
    private embedService: EmbedVideoService,
    private authService: AuthenticationService
  ) {
    this.request$ = this.homeService.request$;
    this.videos$ = this.homeService.videos$;
    this.series$ = this.homeService.series$;
    this.searches$ = this.homeService.searches$;
    this.loading$ = this.homeService.loading$;

    this.request$.subscribe((res: any) => (this.request = res));
    this.loading$.subscribe(res => (this.isLoading = res));

    this.searches$.subscribe(data => {
      if (data) {
        this.videos = data.videos;
        this.series = data.series;
      }
    });

    this.series$.subscribe((response: any) => {
      this.initialSeries = response.data;
    });

    this.authService.user$.subscribe(user => {
      this.plan = user.plan ? user.plan : null;
    });
  }

  ngOnInit() {
    const subscribed = localStorage.getItem('subscribed');
    const auth = JSON.parse(localStorage.getItem('auth'));

    if (subscribed && subscribed === 'yes') {
      document.getElementById('openModalButton').click();
      let datee = new Date(auth.user.subscription.updated_at);
      datee.setMonth(datee.getMonth() + 1);
      let formattedDate = datee.toISOString().slice(0, 10);
      this.modalText = `Your ${auth.user.subscription.plan.frequency} Month Access is activated valid until ${formattedDate}`;
      localStorage.setItem('subscribed', 'no');
    }

    this.isLoading = true;
    // this.homeService.loadVideos();
    this.homeService.loadSeries({
      perPage: 12,
      page: 1,
      search: ''
    });
  }
  ngOnDestroy() {
    this.homeService.reset();
  }

  search() {
    if (this.searchString) {
      this.homeService.reset();
      this.homeService.search(this.searchString);
    }
  }

  reset() {
    this.homeService.reset();
    this.homeService.loadSeries({
      perPage: 12,
      page: 1,
      search: ''
    });
  }

  videoHtml(link: string): void {
    return this.embedService.embed(link, {
      query: { controls: 0 },
      attr: { width: '100%', height: '280' }
    });
  }

  scrollEvent = (event: any): void => {
    const pos =
      event.target.scrollingElement.scrollTop +
      event.target.scrollingElement.clientHeight;
    const max = document.documentElement.offsetHeight;
    console.log(this.request);
    if (
      Math.ceil(pos) >= max &&
      !this.isLoading &&
      this.request.total > this.request.perPage * this.request.page
    ) {
      const request = {
        perPage: 12,
        page: this.request.page + 1,
        search: this.searchString,
        ref: 'paginate'
      };
      this.homeService.loadSeries(request);
    }
  };

  planStatus(plan: any) {
    if (isEmpty(plan)) {
      return false;
    }

    if (plan.status === 'LAPSED' || plan.status === 'CANCELLED') {
      return false;
    }

    return true;
  }

  ngAfterViewInit(): void {
    document.addEventListener('scroll', this.scrollEvent, true);
  }
}
