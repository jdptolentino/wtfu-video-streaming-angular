import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs/internal/Observable';
import * as StoreActions from '../actions/store.action';
import * as FromStore from '../reducers/store.reducer';
import { map } from 'rxjs/internal/operators/map';
import { exhaustMap } from 'rxjs/internal/operators/exhaustMap';
import { Router } from '@angular/router';
import { VideoHttpService } from '@app/shared/services/http/video-http.service';
import { SeriesHttpService } from '@app/shared/services/http/series-http.service';
import { SearchHttpService } from '@app/shared/services/http/search-http.service';

@Injectable()
export class StoreEffects {
  // Get Videos
  @Effect()
  getVideos$: Observable<any> = this.actions$.pipe(
    ofType(StoreActions.loadVideo),
    map(action => action.request),
    exhaustMap(payload =>
      this.videoHttpService
        .all(payload)
        .pipe(
          map((response: any) =>
            StoreActions.loadVideoSuccess({ videos: response.data })
          )
        )
    )
  );

  // Get Series
  @Effect()
  getSeries$: Observable<any> = this.actions$.pipe(
    ofType(StoreActions.loadSeries),
    map(action => action.request),
    exhaustMap(payload =>
      this.seriesHttpService.all(payload).pipe(
        map((response: any) => {
          return StoreActions.loadSeriesSuccess({
            series: response.data,
            page: response.page,
            total: response.total
          });
        })
      )
    )
  );

  // Get search
  @Effect()
  search$: Observable<any> = this.actions$.pipe(
    ofType(StoreActions.search),
    map(action => action.request),
    exhaustMap(payload =>
      this.searchHttpService
        .all(payload)
        .pipe(
          map((response: any) =>
            StoreActions.searchSuccess({ search: response.data })
          )
        )
    )
  );

  constructor(
    private router: Router,
    private actions$: Actions,
    private videoHttpService: VideoHttpService,
    private seriesHttpService: SeriesHttpService,
    private searchHttpService: SearchHttpService
  ) {}
}
