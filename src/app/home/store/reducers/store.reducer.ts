import { Action, createReducer, on } from '@ngrx/store';
import * as Actions from '../actions/store.action';
import StoreState, { initializeState, adapter } from '../store.state';
import { state } from '@angular/animations';

export const intialState = initializeState;

const reducer = createReducer(
  intialState,
  on(Actions.loadVideo, (state: StoreState, {}) => {
    return { ...state, loading: true };
  }),
  on(Actions.loadSeries, (state: StoreState, { request }) => {
    const mergedRequest = request
      ? { ...state.request, ...request }
      : state.request;
    return { ...state, loading: true, request: mergedRequest };
  }),
  on(Actions.search, (state: StoreState, {}) => {
    return { ...state, loading: true };
  }),
  on(
    Actions.loadSeriesSuccess,
    (state: StoreState, { series, page, total }) => {
      state = {
        ...state,
        series,
        request: {
          ...state.request,
          page,
          total
        },
        loading: false
      };
      return adapter.addMany(series, state);
    }
  ),
  on(Actions.searchSuccess, (state: StoreState, { search }) => ({
    ...state,
    search: search,
    loading: false
  })),
  on(Actions.reset, (state: StoreState, {}) => {
    return { ...intialState };
  })
);

export function StoreReducer(state: StoreState | undefined, action: Action) {
  return reducer(state, action);
}
