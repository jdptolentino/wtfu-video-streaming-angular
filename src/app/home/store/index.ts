import { createSelector, createFeatureSelector } from '@ngrx/store';
import StoreState from './store.state';
import * as fromFeature from './reducers/store.reducer';
import { adapter } from './store.state';

export interface FeatureModuleState {
  home: StoreState;
}

export const reducers = {
  home: fromFeature.StoreReducer
};

export interface AppState extends StoreState {
  home: FeatureModuleState;
}

export const selectFeatureModuleState = createFeatureSelector<
  FeatureModuleState
>('homeModule');
export const selectFeatureState = createSelector(
  selectFeatureModuleState,
  (state: FeatureModuleState) => state.home
);
export const getVideo = createSelector(
  selectFeatureState,
  (state: StoreState) => state.videos
);
export const getLoading = createSelector(
  selectFeatureState,
  (state: StoreState) => state.loading
);
export const getSeries = createSelector(
  selectFeatureState,
  (state: StoreState) => state.series
);
export const getSearches = createSelector(
  selectFeatureState,
  (state: StoreState) => state.search
);
export const getRequest = createSelector(
  selectFeatureState,
  (state: StoreState) => state.request
);
export const { selectAll: selectAllData } = adapter.getSelectors(
  selectFeatureState
);
