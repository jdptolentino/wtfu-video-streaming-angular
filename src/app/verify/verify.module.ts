import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { VerifyRoutingModule } from './verify-routing.module';
import { VerifyComponent } from './verify.component';

import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatCardModule } from '@angular/material/card';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@app/shared';

@NgModule({
  imports: [
    CommonModule,
    MatSnackBarModule,
    MatCardModule,
    ReactiveFormsModule,
    TranslateModule,
    NgbModule,
    RouterModule,
    VerifyRoutingModule,
    SharedModule
  ],
  declarations: [VerifyComponent]
})
export class VerifyModule {}
