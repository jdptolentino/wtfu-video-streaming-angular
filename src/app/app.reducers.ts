import { MetaReducer, ActionReducerMap, ActionReducer } from '@ngrx/store';
import { storeFreeze } from 'ngrx-store-freeze';
import { environment } from '@env/environment';

import { localStorageSync } from 'ngrx-store-localstorage';
import * as fromRouter from '@ngrx/router-store';
import { RouterStateUrl } from './shared/utils/router';

// Initial state of the application
export interface State {
  router: fromRouter.RouterReducerState<RouterStateUrl>;
}

// Inital reducer of the application
export const reducers: ActionReducerMap<State> = {
  router: fromRouter.routerReducer
};

// Set local storage
export function localStorageSyncReducer(
  reducer: ActionReducer<any>
): ActionReducer<any> {
  return localStorageSync({ keys: [{ auth: 'asd' }], rehydrate: true })(
    reducer
  );
}

// Setup meta reducers
export const metaReducers: MetaReducer<State>[] = !environment.production
  ? [storeFreeze, localStorageSyncReducer]
  : [localStorageSyncReducer];
