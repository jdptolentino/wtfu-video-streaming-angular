import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { EffectsModule } from '@ngrx/effects';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { StoreModule } from '@ngrx/store';
import { StoreReducer } from '@app/core/authentication/store/reducers/store.reducer';
import { StoreEffects } from '@app/core/authentication/store/effects/store.effect';
import { LoginHttpService } from '@app/shared/services/http/login-http.service';

import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatCardModule } from '@angular/material/card';
import { RouterModule } from '@angular/router';
import { ProfileHttpService } from '@app/shared/services/http/profile-http.service';
import { SharedModule } from '@app/shared';

@NgModule({
  imports: [
    CommonModule,
    MatSnackBarModule,
    MatCardModule,
    ReactiveFormsModule,
    TranslateModule,
    NgbModule,
    RouterModule,
    LoginRoutingModule,
    SharedModule,
    StoreModule.forRoot({ auth: StoreReducer }),
    EffectsModule.forFeature([StoreEffects])
  ],
  declarations: [LoginComponent],
  providers: [LoginHttpService, ProfileHttpService]
})
export class LoginModule {}
