import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { environment } from '@env/environment';
import {
  Logger,
  I18nService,
  AuthenticationService,
  untilDestroyed
} from '@app/core';
import { Observable } from 'rxjs';

const log = new Logger('Login');

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  version: string | null = environment.version;
  error: string | undefined;
  loginForm!: FormGroup;
  loading$: Observable<boolean>;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private i18nService: I18nService,
    private authenticationService: AuthenticationService
  ) {
    this.loading$ = this.authenticationService.loading$;
    this.createForm();
  }

  ngOnInit() {
    this.route.queryParams.subscribe(param => {
      console.log(param);
      if (param.token) {
        this.authenticationService.loginWithToken(param.token);
      }
      if (param.token1) {
        localStorage.setItem('subscribed', 'yes');
        this.authenticationService.loginWithToken(param.token1);
      }
    });
  }

  ngOnDestroy() {}

  login() {
    this.authenticationService.login(this.loginForm.value);
  }

  setLanguage(language: string) {
    this.i18nService.language = language;
  }

  get currentLanguage(): string {
    return this.i18nService.language;
  }

  get languages(): string[] {
    return this.i18nService.supportedLanguages;
  }

  private createForm() {
    this.loginForm = this.formBuilder.group({
      email: [{ value: '', disabled: false }, Validators.required],
      password: [{ value: '', disabled: false }, Validators.required],
      remember: true
    });
  }
}
