import { Component, OnInit } from '@angular/core';
import { TransactionsService } from './transactions.service';
import { Observable } from 'rxjs';
import { Transaction } from '@app/shared/models/db/transaction.model';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss']
})
export class TransactionsComponent implements OnInit {
  transactions$: Observable<Transaction[]>;
  constructor(private transactionService: TransactionsService) {
    this.transactions$ = this.transactionService.transactions$;
    this.transactions$.subscribe(r => console.log(r));
  }

  ngOnInit() {
    this.transactionService.loadTransactions();
  }
}
