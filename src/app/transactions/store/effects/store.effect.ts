import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs/internal/Observable';
import * as StoreActions from '../actions/store.action';
import * as FromStore from '../reducers/store.reducer';
import { map } from 'rxjs/internal/operators/map';
import { exhaustMap } from 'rxjs/internal/operators/exhaustMap';
import { TransactionHttpService } from '@app/shared/services/http/transaction-http.service';

@Injectable()
export class StoreEffects {
  // Get Videos
  @Effect()
  getVideos$: Observable<any> = this.actions$.pipe(
    ofType(StoreActions.loadTransactions),
    map(action => action.request),
    exhaustMap(payload =>
      this.transactionHttpService
        .all(payload)
        .pipe(
          map((response: any) =>
            StoreActions.loadTransactionsSuccess({ response })
          )
        )
    )
  );

  constructor(
    private actions$: Actions,
    private transactionHttpService: TransactionHttpService
  ) {}
}
