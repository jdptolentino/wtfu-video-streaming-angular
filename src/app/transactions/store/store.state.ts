import { Transaction } from '@app/shared/models/db/transaction.model';
import { HttpQueryRequest } from '@app/shared/models/http/http-query-request.model';
import { HttpQueryResponse } from '@app/shared/models/http/http-query-response.model';

export default class StoreState {
  loading: boolean;
  loadingRef: string;
  processing: boolean;
  data: Transaction[];
  request: HttpQueryRequest;
  response: HttpQueryResponse<Transaction>;
}

export const initializeState = (): StoreState => {
  return {
    loading: false,
    loadingRef: null,
    processing: false,
    data: [],
    request: {
      page: 1,
      perPage: 0
    },
    response: {
      total: 0,
      page: 1,
      perPage: 0,
      data: []
    }
  };
};
