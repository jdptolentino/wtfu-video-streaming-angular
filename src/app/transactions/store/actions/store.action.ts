import { createAction, props } from '@ngrx/store';
import { HttpQueryRequest } from '@app/shared/models/http/http-query-request.model';
import { HttpQueryResponse } from '@app/shared/models/http/http-query-response.model';
import { Transaction } from '@app/shared/models/db/transaction.model';

export const loadTransactions = createAction(
  '[Transactions] Load Transactions',
  props<{ request: HttpQueryRequest }>()
);

export const loadTransactionsSuccess = createAction(
  '[Transactions] Load Transactions Success',
  props<{ response: HttpQueryResponse<Transaction> }>()
);
