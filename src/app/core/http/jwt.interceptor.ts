import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor() {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const auth = JSON.parse(localStorage.getItem('auth'));

    if (auth && auth.token !== null) {
      let userHeader = {};

      if (!auth.user) {
        userHeader = {
          Authorization: `Bearer ${auth.token}`
        };
      } else {
        userHeader = {
          Authorization: `Bearer ${auth.token}`,
          'X-User-ID': `${auth.user.id}`
        };
      }
      request = request.clone({
        setHeaders: userHeader
      });
    }

    return next.handle(request);
  }
}
