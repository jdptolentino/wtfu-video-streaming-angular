import { Injectable } from '@angular/core';
import {
  Router,
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';

import { Logger } from '../logger.service';
import { CredentialsService } from './credentials.service';
import { Observable } from 'rxjs';
import { LoginHttpService } from '@app/shared/services/http/login-http.service';
import { take, map, first } from 'rxjs/operators';
import { isEmpty } from 'lodash';
import { Store } from '@ngrx/store';
import StoreState from './store/store.state';
import * as StoreActions from './store/actions/store.action';
import { AuthenticationService } from './authentication.service';

const log = new Logger('AuthenticationGuard');

@Injectable({
  providedIn: 'root'
})
export class NoSubscriptionGuard implements CanActivate {
  constructor(
    private router: Router,
    private store: Store<{ auth: StoreState }>,
    private authService: AuthenticationService
  ) {}

  canActivate(): Observable<boolean> | Promise<boolean> | boolean {
    return this.authService.getUser().pipe(
      first(),
      map((auth: any) => {
        if (!isEmpty(auth)) {
          this.store.dispatch(
            StoreActions.setUser({ token: auth.token, user: auth.user })
          );
          return true;
        }

        this.router.navigate(['/login']);
        return false;
      })
    );
  }
}
