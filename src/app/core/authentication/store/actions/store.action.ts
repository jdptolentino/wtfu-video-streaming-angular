import { createAction, props } from '@ngrx/store';
import {
  AuthLogin,
  AuthRegister,
  AuthForgotPassword
} from '@app/shared/models/view/auth.model';
import { User } from '@app/shared/models/db/user.model';

export const login = createAction('[Auth] Login', props<{ data: AuthLogin }>());
export const loginWithToken = createAction(
  '[Auth] Login with token',
  props<{ token: string }>()
);

export const update = createAction(
  '[Auth] Update',
  props<{ payload: { id: number; user: User } }>()
);

export const register = createAction(
  '[Auth] Register',
  props<{ data: AuthRegister }>()
);
export const registerSuccess = createAction(
  '[Auth] Register Success',
  props<{}>()
);

export const forgot = createAction(
  '[Auth] Forgot Password',
  props<{ data: AuthForgotPassword }>()
);
export const forgotSuccess = createAction(
  '[Auth] Forgot Password Success',
  props<{}>()
);
export const find = createAction(
  '[Auth] Find Token',
  props<{ payload: { token: string } }>()
);
export const findSuccess = createAction(
  '[Auth] Find Token Success',
  props<{ email: string }>()
);
export const reset = createAction('[Auth] reset', props<{ data: any }>());
export const resetSuccess = createAction('[Auth] reset', props<{}>());

export const setToken = createAction(
  '[Auth] Set Token',
  props<{ token: string; user: User }>()
);

export const setUser = createAction(
  '[Auth] Set User',
  props<{ token: string; user: User }>()
);

export const setUserData = createAction(
  '[Auth] Set User Data',
  props<{ user: User }>()
);

export const setErrors = createAction(
  '[Auth] Set Errors',
  props<{ errors: any }>()
);
export const resetErrors = createAction('[Auth] Reset Errors', props<{}>());
export const failed = createAction('[Auth] Failed', props<{ payload: any }>());
