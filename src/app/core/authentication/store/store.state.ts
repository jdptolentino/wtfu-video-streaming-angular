import { User } from '@app/shared/models/db/user.model';

export default class StoreState {
  token: string;
  user: User;
  loading: boolean;
  errors: any;
  forgotPasswordData: {
    email: string;
  };
}

export const initializeState = (): StoreState => {
  return {
    token: '',
    user: {},
    loading: false,
    errors: {},
    forgotPasswordData: {
      email: ''
    }
  };
};
