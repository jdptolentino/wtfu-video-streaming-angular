import { createSelector, createFeatureSelector } from '@ngrx/store';
import StoreState from './store.state';

export const selectFeatureModuleState = createFeatureSelector<StoreState>(
  'auth'
);

export const getLoading = createSelector(
  selectFeatureModuleState,
  (state: StoreState) => state.loading
);
export const getUser = createSelector(
  selectFeatureModuleState,
  (state: StoreState) => state.user
);
export const getForgotData = createSelector(
  selectFeatureModuleState,
  (state: StoreState) => state.forgotPasswordData
);
export const getErrors = createSelector(
  selectFeatureModuleState,
  (state: StoreState) => state.errors
);
