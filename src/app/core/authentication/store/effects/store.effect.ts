import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs/internal/Observable';
import { ActionWithPayloadPrefix } from '@app/shared/models/utils/action-with-prefix.model';
import * as StoreActions from '../actions/store.action';
import * as FromStore from '../reducers/store.reducer';
import { map } from 'rxjs/internal/operators/map';
import { exhaustMap } from 'rxjs/internal/operators/exhaustMap';
import { Store } from '@ngrx/store';
import { LoginHttpService } from '@app/shared/services/http/login-http.service';
import { Router } from '@angular/router';
import { tap, catchError } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';
import { of } from 'rxjs/internal/observable/of';
import { RegisterHttpService } from '@app/shared/services/http/register-http.service';
import { ProfileHttpService } from '@app/shared/services/http/profile-http.service';
import { ForgotHttpService } from '@app/shared/services/http/forgot-http.service';

@Injectable()
export class StoreEffects {
  // Login
  @Effect()
  login$: Observable<any> = this.actions$.pipe(
    ofType(StoreActions.login),
    map(action => action.data),
    exhaustMap(payload =>
      this.loginHttpService
        .login({ email: payload.email, password: payload.password })
        .pipe(
          map((response: any) =>
            StoreActions.setToken({
              token: response.token,
              user: response.user
            })
          ),
          catchError((response: any) =>
            of(StoreActions.failed({ payload: response }))
          )
        )
    )
  );

  // Login with token
  @Effect()
  loginWithToken$: Observable<any> = this.actions$.pipe(
    ofType(StoreActions.loginWithToken),
    map(action => action.token),
    exhaustMap(payload =>
      this.loginHttpService.loginWithToken({ token: payload }).pipe(
        map((response: any) =>
          StoreActions.setToken({ token: response.token, user: response.user })
        ),
        catchError((response: any) =>
          of(StoreActions.failed({ payload: response }))
        )
      )
    )
  );

  // Set Token and Access
  @Effect({ dispatch: false })
  setToken$ = this.actions$.pipe(
    ofType(StoreActions.setToken),
    tap(action => {
      const auth = JSON.stringify({ token: action.token, user: action.user });
      localStorage.setItem('auth', auth);
      this.router.navigate(['/home']);
    })
  );

  // Register
  @Effect()
  register$: Observable<any> = this.actions$.pipe(
    ofType(StoreActions.register),
    map(action => action.data),
    exhaustMap(payload =>
      this.registerHttpService.register(payload).pipe(
        map((response: any) => StoreActions.registerSuccess({})),
        catchError((response: any) =>
          of(StoreActions.setErrors({ errors: response }))
        )
      )
    )
  );

  // Forgot
  @Effect()
  forgot$: Observable<any> = this.actions$.pipe(
    ofType(StoreActions.forgot),
    map(action => action.data),
    exhaustMap(payload =>
      this.forgotHttpService.forgot(payload).pipe(
        map((response: any) => StoreActions.forgotSuccess({})),
        catchError((response: any) => {
          return of(StoreActions.failed({ payload: response }));
        })
      )
    )
  );

  // Set Forgot Success
  @Effect({ dispatch: false })
  forgotSuccess$ = this.actions$.pipe(
    ofType(StoreActions.forgotSuccess),
    tap(action => {
      this.snackbar.open('We have e-mailed your password reset link!', '', {
        duration: 2000,
        verticalPosition: 'top',
        horizontalPosition: 'center',
        panelClass: 'custome-toast'
      });
    })
  );

  // Set Register Success
  @Effect({ dispatch: false })
  registerSuccess$ = this.actions$.pipe(
    ofType(StoreActions.registerSuccess),
    tap(action => {
      this.router.navigate(['/note']);
    })
  );

  // Find
  @Effect()
  find$: Observable<any> = this.actions$.pipe(
    ofType(StoreActions.find),
    map(action => action.payload),
    exhaustMap(payload =>
      this.forgotHttpService.find(payload.token).pipe(
        map((response: any) =>
          StoreActions.findSuccess({ email: response.email })
        ),
        catchError((response: any) => {
          return of(StoreActions.failed({ payload: response }));
        })
      )
    )
  );

  // Reset
  @Effect()
  reset$: Observable<any> = this.actions$.pipe(
    ofType(StoreActions.reset),
    map(action => action.data),
    exhaustMap(payload =>
      this.forgotHttpService
        .reset({
          email: payload.email,
          password: payload.password,
          password_confirmation: payload.password,
          token: payload.token
        })
        .pipe(
          map((response: any) =>
            StoreActions.setToken({
              token: response.token,
              user: response.user
            })
          ),
          catchError((response: any) =>
            of(StoreActions.failed({ payload: response }))
          )
        )
    )
  );

  // Set Forgot Success
  @Effect({ dispatch: false })
  resetSuccess$ = this.actions$.pipe(
    ofType(StoreActions.resetSuccess),
    tap(action => {
      this.snackbar.open('Password successfully updated!', '', {
        duration: 2000,
        verticalPosition: 'top',
        horizontalPosition: 'center',
        panelClass: 'custome-toast'
      });
      this.router.navigate(['/login']);
    })
  );

  // Update
  @Effect({})
  update$: Observable<any> = this.actions$.pipe(
    ofType(StoreActions.update),
    map(action => action.payload),
    exhaustMap(payload =>
      this.profileHttpService.update(payload.id, payload.user).pipe(
        map((response: any) =>
          StoreActions.setUserData({ user: response.data })
        ),
        catchError((response: any) =>
          of(StoreActions.setErrors({ errors: response }))
        )
      )
    )
  );

  // Failed
  @Effect({ dispatch: false })
  failed$ = this.actions$.pipe(
    ofType(StoreActions.failed),
    map(action => action.payload),
    tap((payload: any) => {
      this.snackbar.open(payload, '', {
        duration: 2000,
        verticalPosition: 'top',
        horizontalPosition: 'center',
        panelClass: 'custome-toast'
      });
    })
  );

  constructor(
    private router: Router,
    private actions$: Actions,
    private loginHttpService: LoginHttpService,
    private registerHttpService: RegisterHttpService,
    private forgotHttpService: ForgotHttpService,
    private profileHttpService: ProfileHttpService,
    private snackbar: MatSnackBar
  ) {}
}
